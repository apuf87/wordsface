# Words watch face with configuration #

This project is an example of an implementation of a watch face for *Android wear* platform. There is a configuration application for Android phones, by which you can change colors or shape of watch face.

## About

**Text watch face with a simple interface**

The watch face is made up by randomly generated letters and at randomly generated positions is an actual time with an accuracy of about 5 minutes. The watch face offers a quick overview of the time.

Using the configuration application you can change colors of the dial and text. This watch face can be used with booth square and round Android watch.

This application is fully in English and Czech.

Application can be downloaded from [Google Play](https://play.google.com/store/apps/details?id=cz.dusanjencik.wordswatchface).